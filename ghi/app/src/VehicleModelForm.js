import React, { useState, useEffect } from 'react';

function VehicleModelForm(props) {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    async function fetchManufacturers() {
      const url = 'http://localhost:8100/api/manufacturers/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    }
    fetchManufacturers();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name: name,
      picture_url: pictureUrl,
      manufacturer_id: manufacturerId,
    };

    const vehicleModelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(vehicleModelUrl, fetchConfig);
    if (response.ok) {
      const newVehicleModel = await response.json();
      //console.log(newVehicleModel);

      setName('');
      setPictureUrl('');
      setManufacturerId('');
    }
  }

  function handleNameChange(event) {
    const value = event.target.value;
    setName(value);
  }

  function handlePictureUrlChange(event) {
    const value = event.target.value;
    setPictureUrl(value);
  }

  function handleManufacturerIdChange(event) {
    const value = event.target.value;
    setManufacturerId(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Vehicle Model</h1>
          <form onSubmit={handleSubmit} id="create-vehicle-model-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Model name..." required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL (Less than 600 characters)</label>
            </div>
            <div className="mb-3">
              <select value={manufacturerId} onChange={handleManufacturerIdChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                <option value="">Choose a manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default VehicleModelForm;
