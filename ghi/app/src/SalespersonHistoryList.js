import React, { useState, useEffect } from "react";

function SalespersonHistoryList(props) {
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [specificSalespersonSales, setSpecificSales] = useState([]);





  const getSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
      //console.log("Salespeople", data);
    }
  };


  const getSpecificSales = async () => {
    const url = "http://localhost:8090/api/sales/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const specificSalespersonSales = data.sales.filter(sale => sale.salesperson.id === parseInt(salesperson));
      setSpecificSales(specificSalespersonSales);
      //console.log("Sales", specificSalespersonSales);
      //console.log('Data', data);
      //console.log('salesperson', salesperson);
    }
  };

  useEffect(() => {
    getSpecificSales();
  }, [salesperson]);

  useEffect(() => {
    getSalespeople();
  }, []);



  function handleSalespersonChange(event) {
    const value = event.target.value;
    setSalesperson(value);
  }



  return (
    <>
      <h1>Salesperson History</h1>
      <div className="mb-3">
        <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
            <option value="">Choose a Salesperson</option>
            {salespeople.map(salesperson => {
                return (
                <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name} {salesperson.last_name}
                </option>
                );
            })}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {specificSalespersonSales.map((sale, index) => {
            return (
              <tr key={sale.id + index}>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalespersonHistoryList;
