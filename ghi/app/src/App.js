import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './AutomobileList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import VehicleModelList from './VehicleModelList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import SalespersonForm from './SalespersonForm';
import SalespeopleList from './SalespeopleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import SalespersonHistoryList from './SalespersonHistoryList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />}/>
          <Route path="manufacturers" element={<ManufacturerList />}/>
          <Route path="models" element={<VehicleModelList />}/>
          <Route path="automobiles" element={<AutomobileList />}/>
          <Route path="models/create" element={<VehicleModelForm />}/>
          <Route path="automobiles/create" element={<AutomobileForm />}/>
          <Route path="salespeople/create" element={<SalespersonForm />}/>
          <Route path="salespeople" element={<SalespeopleList />}/>
          <Route path="customers/create" element={<CustomerForm />}/>
          <Route path="customers" element={<CustomerList />}/>
          <Route path="sales/create" element={<SaleForm />}/>
          <Route path="sales" element={<SaleList />}/>
          <Route path="sales/history" element={<SalespersonHistoryList />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
