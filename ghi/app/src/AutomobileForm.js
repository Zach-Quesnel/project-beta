import React, { useState, useEffect } from 'react';

function AutomobileForm(props) {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);

  useEffect(() => {
    async function fetchModels() {
      const url = 'http://localhost:8100/api/models/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      }
    }
    fetchModels();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      color: color,
      year: year,
      vin: vin,
      model_id: model,
    };
    //console.log('Data', data);

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      //console.log(newAutomobile);

      setColor('');
      setYear('');
      setVin('');
      setModel('');
    }
  }

  function handleColorChange(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleYearChange(event) {
    const value = event.target.value;
    setYear(value);
  }

  function handleVinChange(event) {
    const value = event.target.value;
    setVin(value);
  }

  function handleModelChange(event) {
    const value = event.target.value;
    setModel(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an Automobile to Inventory</h1>
          <form onSubmit={handleSubmit} id="create-automobile-model-form">
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color..." required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={year} onChange={handleYearChange} placeholder="Year..." required type="text" name="year" id="year" className="form-control" />
              <label htmlFor="year">Year (YYYY)</label>
            </div>
            <div className="form-floating mb-3">
              <input value={vin} onChange={handleVinChange} placeholder="VIN..." required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Vin</label>
            </div>
            <div className="mb-3">
              <select value={model} onChange={handleModelChange} required name="model" id="model" className="form-select">
                <option value="">Choose a model</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default AutomobileForm;
