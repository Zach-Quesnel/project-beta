from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField()

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length =200)
    employee_id = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length =200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=30)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

class Sale(models.Model):
    price = models.CharField(max_length=200)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "sales",
        on_delete=models.CASCADE,
        null=True,
    )

    salesperson = models.ForeignKey(
        Salesperson,
        related_name = "salesperson",
        on_delete=models.CASCADE,
        null=True,
    )

    customer = models.ForeignKey(
        Customer,
        related_name = "customer",
        on_delete=models.CASCADE,
        null=True,
    )
