import React, { useState, useEffect } from 'react';



function AutomobileList(props) {
  const [automobiles, setAutomobiles] = useState([]);

  const handleDelete = async (value) => {
    const automobileUrl = `http://localhost:8100/api/automobiles/${value.vin}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      getAutomobiles();
      //console.log("Delete Successful");
    }
  }




  const getAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
      //console.log('Automobiles', data);
    }
  }


  useEffect(() => {
      getAutomobiles();
  }, []);



    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
            {/* <th>Delete</th> */}
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => {
            return (
              <tr key={automobile.id + automobile.vin}>
                <td>{ automobile.vin }</td>
                <td>{ automobile.color }</td>
                <td>{ automobile.year }</td>
                <td>{ automobile.model.name }</td>
                <td>{ automobile.model.manufacturer.name }</td>
                <td>{ automobile.sold.toString() }</td>
                {/* <td><button onClick={() => handleDelete(automobile)}>Delete</button></td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default AutomobileList;
