import React, { useState, useEffect } from "react";

function CustomerList(props) {
  const [customers, setCustomers] = useState([]);

  const handleDelete = async (value) => {
    const customerUrl = `http://localhost:8090/api/customers/${value.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      getCustomers();
      //console.log("Delete Successful");
    }
  };

  const getCustomers = async () => {
    const url = "http://localhost:8090/api/customers/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
      //console.log("customers", data);
    }
  };

  useEffect(() => {
    getCustomers();
  }, []);

  return (
    <>
      <h1>Customers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer, index) => {
            return (
              <tr key={customer.id + index}>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
                {/* <td>
                  <button onClick={() => handleDelete(customer)}>Delete</button>
                </td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default CustomerList;
