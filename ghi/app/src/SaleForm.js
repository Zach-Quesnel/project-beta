import React, { useState, useEffect } from 'react';

function SaleForm(props) {
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [automobileVin, setAutomobileVin] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salesperson, setSalesperson] = useState('');
  const [salespeople, setSalespeople] = useState([]);
  const [price, setPrice] = useState('');
  const [formSubmitted, setFormSubmitted] = useState(false);

  useEffect(() => {
    async function fetchCustomers() {
      const url = 'http://localhost:8090/api/customers/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      }
    }
    fetchCustomers();
  }, [formSubmitted])


  useEffect(() => {
    async function fetchSalespeople() {
      const url = 'http://localhost:8090/api/salespeople/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      }
    }
    fetchSalespeople();
  }, [formSubmitted])


  useEffect(() => {
    async function fetchAutomobiles() {
      const url = 'http://localhost:8100/api/automobiles/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        const notSoldAutomobiles = data.autos.filter(automobile => automobile.sold !== true);
        setAutomobiles(notSoldAutomobiles);
      }
    }
    fetchAutomobiles();
  }, [formSubmitted])


  async function handleSubmit(event) {
    event.preventDefault();

    const soldData = {
        sold: true
    }

    const data = {
      price: price,
      automobile: automobileVin,
      salesperson: salesperson,
      customer: customer,
    };
    //console.log('Data', data);
    //console.log('salesperson', salesperson);
    //console.log('customer', customer);




    const soldUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`
    const fetchConfigPut = {
        method: "put",
        body: JSON.stringify(soldData),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const soldResponse = await fetch(soldUrl, fetchConfigPut);
    if (soldResponse.ok) {
        const soldAutomobile = await soldResponse.json();
        //console.log('soldAutomobile', soldAutomobile);
        //console.log(automobileVin)
    }





    const saleUrl = 'http://localhost:8090/api/sales/';
    const fetchConfigPost = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(saleUrl, fetchConfigPost);
    if (response.ok) {
      const newSale = await response.json();
      //console.log(newSale);

      setPrice('');
      setAutomobileVin('');
      setSalesperson('');
      setCustomer('');
      setFormSubmitted(!formSubmitted);
    }
  }

  function handlePriceChange(event) {
    const value = event.target.value;
    setPrice(value);
  }

  function handleAutomobileVinChange(event) {
    const value = event.target.value;
    setAutomobileVin(value);
  }

  function handleSalespersonChange(event) {
    const value = event.target.value;
    setSalesperson(value);
  }

  function handleCustomerChange(event) {
    const value = event.target.value;
    setCustomer(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select value={automobileVin} onChange={handleAutomobileVinChange} required name="automobileVin" id="automobileVin" className="form-select">
                <option value="">Choose an automobile VIN</option>
                {automobiles.map(auto => {
                  return (
                    <option key={auto.id} value={auto.vin}>
                      {auto.vin}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a salesperson</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={price} onChange={handlePriceChange} placeholder="Price..." required type="text" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default SaleForm;
