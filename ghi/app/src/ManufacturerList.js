import React, { useState, useEffect } from 'react';



function ManufacturerList(props) {
  const [manufacturers, setManufacturers] = useState([]);

  const handleDelete = async (value) => {
    const manufacturerUrl = `http://localhost:8100/api/manufacturers/${value.id}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      getManufacturers();
      //console.log("Delete Successful");
    }
  }




  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
      //console.log('Manufacturers', data);
    }
  }


  useEffect(() => {
      getManufacturers();
  }, []);



    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            {/* <th>Delete</th> */}
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer, index) => {
            return (
              <tr key={manufacturer.id + index}>
                <td>{ manufacturer.name }</td>
                {/* <td><button onClick={() => handleDelete(manufacturer)}>Delete</button></td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ManufacturerList;
