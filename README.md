# CarCar

Team:

* Zach Quesnel - Sales
* Zach Quesnel - Service (To Be Completed)

## Design
Front-end: React
Back-end: Django
Database: PostgreSQL

This application is intended to organize the logistics of a car dealership. It includes three different microservices (inventory, sales, and service) which use RESTful APIs to send/receive data, and a poller to fetch data.



**Step-by-step Instructions to run the project**
1. Open up your terminal and navigate to a preferred directory.
2. Type this command: `git clone https://gitlab.com/Zach-Quesnel/automobiles-plus.git` to clone the repository to your computer
3. Enter into the project directory after cloning is complete.
4. Open docker desktop.
5. Type these commands in your terminal:
    - `docker volume create beta-data`
    - `docker-compose build`
    - `docker-compose up`
6. Go to `localhost:3000` in your browser to view the application. The servers may take several minutes to bring up.

**Diagram of Project**
![Domain Driven Design Map](/image/DDD.png)


**URLs and Ports for Each Service**

Manufacturers
| Action  | Method  | URL/Port  | JSON  |
|---|---|---|---|
| List manufacturers	  |  GET | http://localhost:8100/api/manufacturers/  |   |
| Create a manufacturer	  | POST  | http://localhost:8100/api/manufacturers/  | { "name": "Chrysler" }  |
| Get a specific manufacturer  | GET  | http://localhost:8100/api/manufacturers/:id/  |   |
| Update a specific manufacturer  | PUT  | http://localhost:8100/api/manufacturers/:id/  |   |
| Delete a specific manufacturer  | DELETE  | http://localhost:8100/api/manufacturers/:id/  |   |

Vehicles
| Action  | Method  | URL/Port  | JSON  |
|---|---|---|---|
| List vehicle models	  |  GET | http://localhost:8100/api/models/  |   |
| Create a vehicle model	  | POST  | 	http://localhost:8100/api/models/  | {"name": "Sebring", "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg", "manufacturer_id": 1}  |
| Get a specific vehicle model  | GET  | http://localhost:8100/api/models/:id/  |   |
| Update a specific vehicle model  | PUT  | http://localhost:8100/api/models/:id/  | {"name": "Sebring", "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"}  |
| Delete a specific vehicle model  | DELETE  | http://localhost:8100/api/models/:id/  |   |

Automobile
| Action  | Method  | URL/Port  | JSON  |
|---|---|---|---|
| List automobiles		  |  GET | http://localhost:8100/api/automobiles/  |   |
| Create an automobile	  | POST  | http://localhost:8100/api/automobiles/  | {"color": "red", "year": 2012, "vin": "1C3CC5FB2AN120174" "model_id": 1}  |
| Get a specific automobile  | GET  | http://localhost:8100/api/automobiles/:vin/  |   |
| Update a specific automobile  | PUT  | 	http://localhost:8100/api/automobiles/:vin/  | { "color": "red", "year": 2012, "sold": true}  |
| Delete a specific automobile  | DELETE  | http://localhost:8100/api/automobiles/:vin/  |   |

Sales
| Action  | Method  | URL/Port  | JSON  |
|---|---|---|---|
| List Salespeople  | GET  | 	http://localhost:8090/api/salespeople/  |   |
| Create a salespeople	  | POST  | http://localhost:8090/api/salespeople/  |  {"first_name": "Nathan", "last_name": "Ryan", "employee_id": "NRyan"} |
| Delete a specific salespeople  | DELETE  | 	http://localhost:8090/api/salespeople/:id/  |   |
| Get specific salespeople  | GET  | 	http://localhost:8090/api/salespeople/:id/  |   |
| List Customer  | GET  | http://localhost:8090/api/customers/  |   |
| Create a Customer  | POST  | http://localhost:8090/api/customers/  | {"first_name": "Zach", "last_name": "Quesnel", "address": "23761 184th Pl NE", "phone_number": "206-826-0278"}  |
| Delete a specific customer  | DELETE  | http://localhost:8090/api/customers/:id/  |   |
| Get a specific customer  | GET  | http://localhost:8090/api/customers/:id/  |   |
| List sales  | GET  | http://localhost:8090/api/sales/  |   |
| Create a sale | POST  |  http://localhost:8090/api/sales/ |  {"price": "40000", "automobile": "4A3AK24F36E026626", "salesperson": 1,	"customer": 1} |
| Delete a sale  | DELETE  | http://localhost:8090/api/sales/:id/  |   |
| Get a sale  | GET  | http://localhost:8090/api/sales/:id/  |   |


Services
| Action  | Method  | URL/Port  | JSON  |
|---|---|---|---|
| List Technicians  | GET  | http://localhost:8080/api/technicians/  |   |
| Create a Technician  | POST  | http://localhost:8080/api/technicians/  |  {"first_name": "Takeya", "last_name": "Rhodes", "employee_id": "TRhodes"} |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id  |   |
| List Appointments  | GET  | http://localhost:8080/api/appointments/  |   |
| Create an Appointment  | POST  | http://localhost:8080/api/appointments/  | {"date_time":"2023-06-26 09:00", "reason":"oil change", "vin": "4A3AK24F36E026626","customer": "Zach Quesnel", "finished":false,"canceled":false,"technician": 1} |
| Delete Appointment  | DELETE  | http://localhost:8080/api/appointments/:id  |  |
| Set appointment status to canceled | PUT  |  http://localhost:8080/api/appointments/:id/cancel | {"canceled": true}  |
| Set appointment status to finished | PUT  |  http://localhost:8080/api/appointments/:id/finish | {"finished": true}  |


### Examples of CRUD

* The following are example cases of API endpoints from insomnia:

* List salespeople - **GET** - "http://localhost:8090/api/salespeople/"
**Returns:**
```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Jake",
			"last_name": "Johnson",
			"employee_id": "JJohnson"
		},
		{
			"href": "/api/salespeople/2/",
			"id": 2,
			"first_name": "Nathan",
			"last_name": "Ryan",
			"employee_id": "NRyan"
		}
	]
}
```
* Get a specific salesperson - **GET** - "http://localhost:8090/api/salespeople/1/"
**Returns:**
```
{
	"href": "/api/salespeople/1/",
	"id": 1,
	"first_name": "Zach",
	"last_name": "Quesnel",
	"employee_id": "ZQuesnel"
}
```
* Create salesperson - **POST** - "http://localhost:8090/api/salespeople/"
**SAMPLE DATA**
```
{
  "first_name": "Nathan",
  "last_name": "Ryan",
  "employee_id": "NRyan"
}
```
**Returns:**
```
{
	"href": "/api/salespeople/2/",
	"id": 2,
	"first_name": "Nathan",
	"last_name": "Ryan",
	"employee_id": "NRyan"
}
```
* Delete a specific salesperson - **DELETE** - "http://localhost:8090/api/salespeople/1/"
**Returns:**
```
{
    "deleted": {
        {
	"href": "/api/salespeople/2/",
	"id": 2,
	"first_name": "Nathan",
	"last_name": "Ryan",
	"employee_id": "NRyan"
}
    }
}
```

* List sales - **GET** - "http://localhost:8090/api/sales/"
**Returns:**
```

{
	"sales": [
		{
			"id": 1,
			"price": "223334",
			"automobile": {
				"vin": "1G1PA5SH6E7186503",
				"sold": true
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Jake",
				"last_name": "Johnson",
				"employee_id": "JJohnson"
			},
			"customer": {
				"href": "/api/customers/1/",
				"id": 1,
				"first_name": "Zach",
				"last_name": "Quesnel",
				"address": "28249 152nd PL SE",
				"phone_number": "203-023-9923"
			}
		},
	]
}
```
* Get a specific sale - **GET** - "http://localhost:8090/api/sales/1/"
**Returns:**
```
{
	"id": 1,
	"price": "300",
	"automobile": {
		"vin": "1C3CC5FB2AN120174",
		"sold": true
	},
	"salesperson": {
		"href": "/api/salespeople/1/",
		"id": 1,
		"first_name": "Zach",
		"last_name": "Quesnel",
		"employee_id": "ZQuesnel"
	},
	"customer": {
		"href": "/api/customers/1/",
		"id": 1,
		"first_name": "Zach",
		"last_name": "Quesnel",
		"address": "14287 192nd Pl SE",
		"phone_number": "206-384-9823"
	}
}
```
* Create a sale - **POST** - "http://localhost:8090/api/sales/"
**SAMPLE DATA**
```
{
  "price": "40400",
  "automobile": "4A3AK24F36E026626",
  "salesperson": 1,
	"customer": 1
}
```
* Delete a sale - **DELETE** - "http://localhost:8090/api/sales/1/"
**Returns:**
```
{
	"deleted": {
		"id": null,
		"price": "435634",
		"automobile": {
			"vin": "4A3AK24F36E026626",
			"sold": true
		},
		"salesperson": {
			"href": "/api/salespeople/2/",
			"id": 2,
			"first_name": "Nathan",
			"last_name": "Ryan",
			"employee_id": "NRyan"
		},
		"customer": {
			"href": "/api/customers/2/",
			"id": 2,
			"first_name": "Nick",
			"last_name": "Yee",
			"address": "23761 184th Pl NE",
			"phone_number": "206-826-0278"
		}
	}
}
```





## Service microservice

Explain your models and integration with the inventory
microservice, here.









## Sales microservice (CRUD Route Documentation & Identification of Value Objects)

The CRUD routes, along with sample posting JSON are in the tables above.

The sales microservice is comprised of four main models. They are AutomobileVO (vin and sold as properties), Salesperson (first name, last name, and employee ID as properties), Customer(first name, last name, address, and phone number as properties), and Sale (automobile, salesperson, customer as foreign keys, and price as a property). The overall purpose of this microservice was to keep track of the sale of automobiles, but also to keep track of employees, and customers as well. To run through the functionality of this microservice, navigate to the create pages and submit the forms in this order until you have completed an automobile sale record (create a manufacturer --> create a model --> create an automobile --> add a customer, add a salesperson, add a sale). Once you are done with that, you can see the associated lists with each form that was submitted. The "add a sale" form features a VIN dropdown that only populates with automobiles that are not yet sold, and the "Salesperson history list" features a list that only shows the sale records of the chosen employee.

There was a poller used in this microservice in order to gain access to the autmobile model in the inventory microservice. This was done so that the data for the automobileVO could be populated in the sales microservice.
