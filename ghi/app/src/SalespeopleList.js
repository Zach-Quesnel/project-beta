import React, { useState, useEffect } from "react";

function SalespeopleList(props) {
  const [salespeople, setSalespeople] = useState([]);

  const handleDelete = async (value) => {
    const salespersonUrl = `http://localhost:8090/api/salespeople/${value.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(salespersonUrl, fetchConfig);
    if (response.ok) {
      getSalespeople();
      //console.log("Delete Successful");
    }
  };

  const getSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
      //console.log("Salespeople", data);
    }
  };

  useEffect(() => {
    getSalespeople();
  }, []);

  return (
    <>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson, index) => {
            return (
              <tr key={salesperson.id + index}>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                {/* <td>
                  <button onClick={() => handleDelete(salesperson)}>
                    Delete
                  </button>
                </td> */}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalespeopleList;
